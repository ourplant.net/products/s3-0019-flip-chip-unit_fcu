Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/01_operating_manual/S3-0019_15-0525_D_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/01_operating_manual/S3-0019_15-0525_B1_Operating%20Manual.pdf)                      |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/02_assembly_drawing/s3-0019-a_flip_chip_unit_fcu.pdf)                  |
| circuit diagram          | [de](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/03_circuit_diagram/S3-0019-EPLAN-B.pdf)                 |
| maintenance instructions |  [de](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/04_maintenance_instructions/S3-0019_A_Wartungsanweisungen.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/04_maintenance_instructions/S3-0019_A_Maintenance_instructions.pdf)                 |
| spare parts              |  [de](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/05_spare_parts/S3-0019_15-0525_B1_EVL_FLip%20Chip%20Unit.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0019-flip-chip-unit_fcu/-/raw/main/05_spare_parts/S3-0019_15-0525_A1_EVL_engl.pdf)                 |

